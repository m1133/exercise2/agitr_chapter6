Session 4: The launch utility (https://sir.upc.edu/projects/rostutorials)

Exercise 2a: Modify the teleopturtlesim.launch file to substitute the teleop_key node by the pubvel node of package agitr_chapter3 that commands random velocities to the /turtle1/cmd_vel topic (call it randvelturtlesim.launch).

Exercise 2b: Modify the teleopturtlesim2.launch file in order to use the randvelturtlesim.launch file created before, instead of teleopturtlesim.launch (call it randvelturtlesim2.launch).

Exercise 2c: Create a launch file to move the turtle in the turtlesim_node by either commanding the velocities through the keyword using the teleop_key node, or by using the random velocities generated by the pubvel node of package agitr_chapter3. Call it teleopturtlesim4.launch.
